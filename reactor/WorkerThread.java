package reactor;

import reactorapi.*;

public class WorkerThread<T> extends Thread {
	private final EventHandler<T> handler;
	private final BlockingEventQueue<Object> queue;
	
	private boolean Running = true;
	// Additional fields are allowed.

	public WorkerThread(EventHandler<T> eh, BlockingEventQueue<Object> q) {
		handler = eh;
		queue = q;
	}

	public void run()
	{
		// TODO: Implement WorkerThread.run().
		while(Running)
		{
			Handle<T> Handle = handler.getHandle();
			T Obj = Handle.read();						//blocking
			Event<T> E = new Event<T>(Obj,handler);
			try
			{
				queue.put(E);					//put the event in the BlockingEventQue
				if(Obj == null)					//stop reading if null
				{
					cancelThread();
				}
			} catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
				cancelThread();
			}
		}
	}

	public void cancelThread()
	{
		// TODO: Implement WorkerThread.cancelThread().
		Running = false;
	}
}