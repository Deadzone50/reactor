package reactor;

import reactorapi.BlockingQueue;
import java.util.List;
import java.util.Vector;

public class BlockingEventQueue<T> implements BlockingQueue<Event<? extends T>> {
	public BlockingEventQueue(int capacity)
	{
		Data = new Vector<Event<? extends T>>(capacity);
	}

	public int getSize()
	{
		return Data.size();
	}

	public int getCapacity() 
	{
		return Data.capacity();
	}

	public synchronized Event<? extends T> get() throws InterruptedException 
	{
		while(Data.size() <= 0)					//while empty MONITOR
		{
			wait();
		}
		Event<? extends T> Tmp = Data.remove(0);
		notifyAll();							//wake all thread in monitors
		return Tmp;
		
		// TODO: Implement BlockingEventQueue.get().
	}

	public synchronized List<Event<? extends T>> getAll() {
		throw new UnsupportedOperationException(); // Replace this.
		// TODO: Implement BlockingEventQueue.getAll().
	}

	public synchronized void put(Event<? extends T> event) throws InterruptedException
	{
		while(Data.size() >= Data.capacity())	//while full MONITOR
		{
			wait();
		}

		Data.addElement(event);
		notifyAll();							//wake all thread in monitors
		// TODO: Implement BlockingEventQueue.put(Event).
	}
	private Vector<Event<? extends T>> Data;
	// Add other methods and variables here as needed.
}