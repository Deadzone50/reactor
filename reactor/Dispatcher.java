package reactor;

import reactorapi.*;
import java.util.Vector;

public class Dispatcher {
	public Dispatcher() {
		this(10);
	}

	public Dispatcher(int capacity)
	{
		// TODO: Implement Dispatcher(int).
		Que = new BlockingEventQueue<Object>(capacity);
		EventHandlerList = new Vector<EventHandler<?>>();		//vector of registered eventhandlers
		Threads = new Vector<WorkerThread<?>>();							//and their threads
	}
	
	
	public void handleEvents() throws InterruptedException
	{
		// TODO: Implement Dispatcher.handleEvents().
		while(EventHandlerList.size() > 0)
		{
			Event<?> E = select();
			EventHandler<?> h = E.getHandler();
			if(EventHandlerList.indexOf(h) != -1)		//dont dispatch to unregistred handlers
			{
				E.handle();
			}
		}
	}

	public Event<?> select() throws InterruptedException
	{
		// TODO: Implement Dispatcher.select().
		return Que.get();
	}

	public void addHandler(EventHandler<?> h)
	{
		// TODO: Implement Dispatcher.addHandler(EventHandler).
		EventHandlerList.add(h);
		
		WorkerThread<Object> WT = new WorkerThread<Object>((EventHandler<Object>) h, Que);		//create worker thread, can't get rid of this warning
		Threads.add(WT);
		WT.start();
	}

	public void removeHandler(EventHandler<?> h)
	{
		// TODO: Implement Dispatcher.removeHandler(EventHandler).
		int i = EventHandlerList.indexOf(h);
		EventHandlerList.remove(i);
		Threads.get(i).cancelThread();
		Threads.remove(i);
	}
	
	/*public void removeThread(Thread T)
	{
		Threads.remove(T);
	}*/

	// Add methods and fields as needed.
	private BlockingEventQueue<Object> Que;
	private Vector<EventHandler<?>> EventHandlerList;
	private Vector<WorkerThread<?>> Threads;
	
}
