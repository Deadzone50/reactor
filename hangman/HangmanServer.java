package hangman;

import java.io.IOException;
import java.net.Socket;
import java.util.Vector;
import reactor.*;
import reactorapi.EventHandler;

public class HangmanServer
{
	private Vector<Player> Players = new Vector<Player>();
	private int Tries;
	private String WordToGuess;
	private String CurrentGuess;
	private boolean Ended;
	
	Dispatcher D = new Dispatcher();
	TCPHandler TCPH;
	
	public static void main(String[] args)
	{
		String WTG = args[0].toLowerCase();
		int T = Integer.parseInt(args[1]);
		new HangmanServer(WTG, T);
	}
	
	public HangmanServer(String WTG, int T)
	{
		WordToGuess = WTG;
		Tries = T;
		StringBuffer s = new StringBuffer(WordToGuess.length());
		for (int i = 0; i < WordToGuess.length(); i++)
			s.append('-');
		CurrentGuess = s.toString();
		TCPH = new TCPHandler();
		D.addHandler(TCPH);
		try {
			D.handleEvents();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public boolean MakeGuess(char g)
	{
		boolean Found = false;

		char[] CG = CurrentGuess.toCharArray();

		for (int i = 0; i < CurrentGuess.length(); i++)
			if (WordToGuess.charAt(i) == g)
			{
				CG[i] = g;
				Found = true;
			}
		
		CurrentGuess = new String(CG);
		if (!Found)
			Tries--;

		if (Tries == 0 || CurrentGuess.indexOf('-') < 0)
			Ended = true;
		return Found;
	}
	
	public String GetMaskedWord() 
	{
		return CurrentGuess;
	}
	
	public int GetTriesLeft()
	{
		return Tries;
	}
	public boolean GameEnded()
	{
		return Ended;
	}
	/**
	 * Get the status string to sent to newly connected players.
	 * 
	 * @return status string
	 */
	public String GetStatus()
	{
		return CurrentGuess + " " + Tries;
	}

	public Player AddNewPlayer(Socket s)
	{
		Player p = new Player(s);
		Players.add(p);
		return p;
	}
	public void RemovePlayer(Player p)
	{
		Players.remove(p);
	}
	public Vector<Player> GetPlayers()
	{
		return new Vector<Player>(Players);
	}
	public class Player implements EventHandler<String>		//listens for guesses
	{
		public final String Name;
		TCPTextHandle TTH;									//player handle/socket

		Player(Socket s)
		{
			TTH = new TCPTextHandle(s);						//create handle
			Name = TTH.read();								//get name of player
			TTH.write(GetStatus());							//welcome line
		}

		/**
		 * Get the string describing a guess made by this player.
		 * 
		 * @param g
		 *            guess
		 * @return string describing the guess
		 */
		public String GetGuessString(char g)
		{
			return g + " " + GetStatus() + " " + Name;
		}

		public TCPTextHandle getHandle()
		{
			return TTH;
		}

		public void handleEvent(String s)		//Received guess
		{
			if(s.length() == 1)
			{
				char g = s.charAt(0);
				MakeGuess(g);
				for(Player P : Players)
				{
					P.send(GetGuessString(g));
				}
				if(Ended == true)			//close the server
				{
					for(Player P : Players)
					{
						P.close();			//closes connection to players and removes their handlers
					}
					TCPH.close();			//closes connection and removes handler
				}
			}
			else
			{
				TTH.write("invalid guess");
			}
		}
		public void send(String s)				//send message to player
		{
			TTH.write(s);
		}
		public void close()						//close player socket
		{
			TTH.close();
			D.removeHandler(this);
		}
		
	}
	public class TCPHandler implements EventHandler<Socket>		//listen for new players
	{
		AcceptHandle AH;
		public TCPHandler()
		{
			try
			{
				AH = new AcceptHandle();		
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public AcceptHandle getHandle()
		{
			return AH;
		}

		public void handleEvent(Socket s)
		{
			//read name of player + create handler for communication
			//TCPTextHandle TTH = new TCPTextHandle(s);
			//String Name = TTH.read();
			//AddNewPlayer(Name,s);
			AddNewPlayer(s);
			//Players.lastElement().send(GetStatus());		//welcome line
			D.addHandler(Players.lastElement());
		}
		public void close()
		{
			AH.close();
			D.removeHandler(this);
		}
	}
}

	
